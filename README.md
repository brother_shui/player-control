# 玩家控制
## 项目介绍
***[介绍文档](https://juejin.cn/post/7351642274273918986)***
## 代码分支
- master
  - 自实现的玩家第三人称、第一人称控制，及手机端摇杆适配输入
  - ![项目截图](https://gitee.com/brother_shui/player-control/raw/master/screens/master.png)
- base
  - 使用 ecctrl 库 实现玩家第三人称控制，自带摇杆适配（精简无干扰版）
  - ![项目截图](https://gitee.com/brother_shui/player-control/raw/master/screens/base.png)
- game
  - 使用 ecctrl 库人物控制
  - 加载小岛环境
  - 天空、海水
  - bgm、音效
  - 添加socket后端，实现多人在线功能
  - ![项目截图](https://gitee.com/brother_shui/player-control/raw/master/screens/game.png)
- develop
  - 继承game分支，去除多人在线功能
  - 使用 ecctrl 库实现玩家第一人称控制
  - 通过路由控制，加载不同场景
  - 场景之间切换，子场景嵌套进主场景
  - 后期处理效果
  - 调试面板
  - 代码最新版本
  - ...
  - ![项目截图](https://gitee.com/brother_shui/player-control/raw/master/screens/develop.jpg)

## 项目搭建
- 初始化
```sh
  npm create vite@latest playerCtr -- --template react-swc-ts
```
- 添加react-three依赖
```sh
  npm install three @types/three @react-three/fiber
```
- 添加react-three工具包
```shell
  npm install @react-three/drei 
```
- 添加物理效果
```shell
  npm install @react-three/rapier
```


