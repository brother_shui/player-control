import { useEffect, useState } from "react";
import { useProgress } from "@react-three/drei";
import "./index.less";

// 模型加载时显示的页面
function Loading() {
  const { progress } = useProgress();
  const [couldStart, setStart] = useState(false);
  const [load, setLoad] = useState(true);

  useEffect(() => {
    if (progress >= 100) {
      setStart(true);
    }
  }, [progress]);

  function start() {
    setTimeout(() => {
      setLoad(false);
    }, 500);
  }

  return (
    load && (
      <div className={`loading`}>
        <div>{progress.toFixed()}%</div>
        <div className="tubeBox">
          <div className="tube" style={{ width: `${progress}%` }}></div>
        </div>

        {couldStart && (
          <div className="start" onClick={start}>
            开始
          </div>
        )}
      </div>
    )
  );
}
export default Loading;
