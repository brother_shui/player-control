import "./index.less";
import JoyStick from "./joysticks";
import Loading from "./loading";

function Pages() {
  return (
    <div className="pages">
      <JoyStick />
      <Loading />
    </div>
  );
}
export default Pages;
