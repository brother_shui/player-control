import Models from './models'
import Pages from './pages'

function App() {

  return (
    <>
      <Models />
      <Pages />
    </>
  )
}

export default App
