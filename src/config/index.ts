export const DEBUG = window.location.href.includes("debug");
export const POV = window.location.href.includes("pov");

// 玩家初始位置
export const START_POS: [x: number, y: number, z: number] = [2, 2, 2];
// 玩家初始方向
export const START_ROT = [Math.PI / 3, -2.78];
