import { Camera, Group, Object3DEventMap, Mesh, Vector3 } from "three";

/**
 * 处理浮点数
 * 参数：
 * num: 待处理数字
 * digit: 保留小数位，默认3
 */
export function toFixed(num: number, digit = 3) {
  const scale = 10 ** digit;
  return Math.floor(num * scale) / scale;
}

/**
 * 为模型的网格启用阴影
 * 参数：
 * scene: 模型的根节点
 */
export function openShadow(scene: Group<Object3DEventMap>) {
  scene.traverse(
    (obj) => obj instanceof Mesh && (obj.receiveShadow = obj.castShadow = true)
  );
}

/**
 * 获取随机整数
 * 参数：
 * min: 随机范围起始数
 * max: 随机范围结束数
 */
export const randInt = (min: number, max: number) =>
  Math.floor((max - min + 1) * Math.random()) + min;

/**
 * 获取当前相机的看向的位置
 */
export const cameraLookPos = (camera: Camera) =>
  camera.getWorldDirection(new Vector3()).add(camera.position);
