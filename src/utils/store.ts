import { create, StateCreator, StoreApi } from "zustand";
import { subscribeWithSelector } from "zustand/middleware";

/**
 * 封装创建 Store 的逻辑, 页面仅仅监听解构获取的变量（原来会监听所有）
 */
export const createStore = <T extends object>(
  storeDefinition: StateCreator<T>
) => {
  const store = /* @__PURE__ */ create(
    /* @__PURE__ */ subscribeWithSelector(storeDefinition)
  );
  // 返回一个 Proxy，代理 store 的所有方法和状态访问
  return new Proxy(store as (() => T) & T & StoreApi<T>, {
    get(_, prop) {
      // 如果访问的是 store 的方法或属性（如 getState、setState 等），直接返回方法
      const original = store[prop as keyof typeof store];
      if (original) return original;
      // 否则，动态选择器订阅状态属性
      return store((state) => state[prop as keyof T]);
    },
  });
};

/**  调用及使用示例
 //store.ts
interface StoreState {
  count: number;
  updateCount: () => void;
}
const useTest = createStore<StoreState>((set) => ({
  count: 0,
  updateCount: () => set((state) => ({ count: state.count + 1 })),
}));
export default useTest;

// page.tsx
const { count ,setState,updateCount} = useTest;
console.log(count)

// 如果 想恢复到原来的使用，只要调用其方法即可,完全支持之前的写法；同时多了新的变量解构自动订阅的功能
const { count,update } =useTest()

*/
