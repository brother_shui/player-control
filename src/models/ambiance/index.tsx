import { PositionalAudio } from "@react-three/drei";

const audios = {
  beach: './audios/beach.mp3',
  forest: './audios/forest.mp3'
}

// 空间音频
export default function Ambiance({ debug = false }) {
  return (
    <group>
      {/* 沙滩海浪声 */}
      {/* <mesh position={[61, -4, 70]}>
        {debug && (
          <>
            <sphereGeometry args={[30, 64, 64]} />
            <meshStandardMaterial transparent opacity={0.5} />
          </>
        )}
        <PositionalAudio url={audios.beach} distance={30} autoplay loop />
      </mesh>
      <mesh position={[42, -4, -15]}>
        {debug && (
          <>
            <sphereGeometry args={[10, 64, 64]} />
            <meshStandardMaterial transparent opacity={0.5} />
          </>
        )}
        <PositionalAudio url={audios.beach} distance={10} autoplay loop />
      </mesh>
      <mesh position={[-20, -7, -100]}>
        {debug && (
          <>
            <sphereGeometry args={[30, 64, 64]} />
            <meshStandardMaterial transparent opacity={0.5} />
          </>
        )}
        <PositionalAudio url={audios.beach} distance={30} autoplay loop />
      </mesh> */}
      {/* 森林鸟叫声 */}
      <mesh position={[0, 0, 3]}>
        {debug && (
          <>
            <sphereGeometry args={[2, 64, 64]} />
            <meshStandardMaterial transparent opacity={0.5} />
          </>
        )}
        <PositionalAudio url={audios.forest} distance={2} autoplay loop />
      </mesh>
    </group>
  );
}
