import { createStore } from "@/utils/store";
import { Vector3 } from "three";

export const useModels = createStore<ModelsState>(() => ({
  director: undefined,
  action: false,
  cameraTarget: undefined,
}));
export const setModel = useModels.setState;
type ModelsState = {
  action: boolean;
  director: Vector3 | undefined;
  cameraTarget: { position: Vector3; target: Vector3 } | undefined;
};
